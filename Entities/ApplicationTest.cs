﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class ApplicationTest
    {
        [Key]
        public int Id { get; set; }
        
        public DateTime CreateDate { get; set; }

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string TestName { get; set; }        

        public MailTemplate MailTemplate { get; set; }        

        public List<ApplicationUser> Customers { get; set; }
        public List<ApplicationRecepient> Recepients { get; set; }        
        public ApplicationUser Retailer { get; set; }

        public List<ApplicationRecepient> GetRecepientsWhoClicked()
        {
            return Recepients.FindAll(x => x.ClickCount > 0);
        }

        public List<ApplicationRecepient> GetRecepientsWhoOpened()
        {
            return Recepients.FindAll(x => x.OpenCount > 0);
        }
    }
}
