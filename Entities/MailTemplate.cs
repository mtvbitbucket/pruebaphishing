﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities
{
    public class MailTemplate
    {
        [Key]
        public int Id { get; set; }
        public List<ApplicationTest> ApplicationTests { get; set; }

        //Domain the email is sent from
        public string SenderDomain { get; set; }

        //Name of the sender
        public string SenderDomainName { get; set; }

        //HTML Content
        public string HtmlContent { get; set; }

        //Subject of the email
        public string Subject { get; set; }
        public string RedirectUrl { get; set; }
    }

    public interface IMailRecepient
    {
        string GetName();

        string GetEmail();
    }
}