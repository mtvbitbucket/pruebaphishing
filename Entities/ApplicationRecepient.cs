﻿using Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace Entities
{
    public class ApplicationRecepient : IMailRecepient
    {
        [Key]
        public int Id { get; set; }
                
        public ApplicationTest Test { get; set; }
        public string Email { get; set; }
        public int ClickCount { get; set; }
        public int OpenCount { get; set; }
        public int SubmitCount { get; set; }
        public string Name { get; set; }

        public string GetName()
        {
            return Name;
        }

        public string GetEmail()
        {
            return Email;
        }
    }
}