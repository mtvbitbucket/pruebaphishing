namespace PruebaAPI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class redirecturl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MailTemplates", "RedirectUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.MailTemplates", "RedirectUrl");
        }
    }
}
