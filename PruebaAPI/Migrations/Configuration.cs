using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PruebaAPI.Infrastructure;

namespace PruebaAPI.Migrations
{
    using Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PruebaDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PruebaAPI.Infrastructure.PruebaDBContext context)
        {
            //  This method will be called after migrating to the latest version.          

            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new PruebaDBContext()));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new PruebaDBContext()));

            var user = new ApplicationUser()
            {
                UserName = "SuperAdmin",
                Email = "prueba@prueba.dk",
                EmailConfirmed = true,
                FirstName = "Nicolai",
                LastName = "Nielsen",
                Level = 1,
                JoinDate = DateTime.Now.AddYears(-3),
                Company = "Prueba Cybersecurity"
            };

            manager.Create(user, "MySuperP@ssword!");

            if (roleManager.Roles.Count() == 0)
            {
                roleManager.Create(new IdentityRole { Name = "SuperAdmin"});
                roleManager.Create(new IdentityRole { Name = "Admin" });
                roleManager.Create(new IdentityRole { Name = "User" });
                roleManager.Create(new IdentityRole { Name = "Retailer" });
                roleManager.Create(new IdentityRole { Name = "Customer" });
            }


            var adminUser = manager.FindByName("SuperAdmin");

            manager.AddToRoles(adminUser.Id, new string[] {"SuperAdmin", "Admin", "User", "Retailer", "Customer"});
        }
    }
}
