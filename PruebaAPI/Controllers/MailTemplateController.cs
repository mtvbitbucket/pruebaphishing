﻿using Entities;
using PruebaAPI.Infrastructure;
using PruebaAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace PruebaAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/templates")]
    public class MailTemplateController : BaseApiController
    {
        [Route("all")]
        public IHttpActionResult GetTemplates()
        {
            
            try
            {                
                return Ok(this.TheModelFactory.CreateList(this.MailTempManager.GetAll().ToList()));
            }
            catch (Exception e)
            {
                return BadRequest("Error getting template");
            }
        }
        [Route("single/{id}", Name = "FuckingTemplate")]
        public IHttpActionResult GetTemplate(int id)
        {
            try
            {
                return Ok(this.TheModelFactory.Create(this.MailTempManager.GetSingle(id)));
            }
            catch(Exception e)
            {
                return BadRequest("Error getting template");
            }            
        }

        [Authorize(Roles = "Admin")]
        [Route("create")]
        public IHttpActionResult CreateTemplate(TemplateCreateBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }            

            MailTemplate mailTemplate = new MailTemplate()
            {
                HtmlContent = model.HtmlContent,
                SenderDomain = model.SenderDomain,
                SenderDomainName = model.SenderDomainName,
                Subject = model.Subject,
                RedirectUrl = model.RedirectUrl
            };
                      
            try
            {
                var created = this.MailTempManager.Create(mailTemplate);                

                return CreatedAtRoute("FuckingTemplate", new { controller = "templates", id = created.Id }, this.TheModelFactory.Create(created));
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [Authorize(Roles = "Admin")]
        [Route("delete")]
        [ResponseType(typeof(MailTemplate))]
        [HttpDelete]
        public IHttpActionResult DeleteMailTemplate(int id)
        {

            MailTemplate template = this.MailTempManager.GetSingle(id);            
            if (template == null)
            {
                return NotFound();
            }

            this.MailTempManager.Delete(template.Id);

            return Ok(template);
        }

        [Route("update/{id}")]
        [ResponseType(typeof(void))]
        [HttpPut]
        public IHttpActionResult EditMailTemplate(int id, TemplateEditBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.Id)
            {
                return BadRequest();
            }
            MailTemplate template = new MailTemplate()
            {
                Id = model.Id,
                HtmlContent = model.HtmlContent,
                SenderDomain = model.SenderDomain,
                SenderDomainName = model.SenderDomainName,
                Subject = model.Subject,
                RedirectUrl = model.RedirectUrl
            };

            try
            {
                var templateToEdit = this.MailTempManager.Edit(model.Id, template);
                return Ok(templateToEdit);
            }
            catch (Exception e)
            {
                return BadRequest("Error editing template");
            }
        }
    }
}
