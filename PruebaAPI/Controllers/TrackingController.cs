﻿using Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PruebaAPI.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/mail")]
    public class TrackingController : BaseApiController
    {        
        [Route("click/{id}")]
        [HttpGet]
        public IHttpActionResult click(int id, string redirect)
        {
            ApplicationRecepient rec = this.AppRecManager.GetSingle(id);
            this.AppRecManager.addClick(rec.Id);
            ApplicationTest test = this.AppTestManager.GetTestByRecepientId(id);
            MailTemplate template = this.MailTempManager.GetSingle(test.MailTemplate.Id);

            string url = redirect;

            System.Uri uri = new System.Uri(url);
            return Redirect(uri);            
        }
        [Route("open/{id}")]
        [HttpGet]
        public IHttpActionResult open(int id)
        {
            this.AppRecManager.addOpen(id);            
            return Ok();
        }
        [Route("submit")]
        public IHttpActionResult submit(int id)
        {
            return Ok();
        }
    }
}
