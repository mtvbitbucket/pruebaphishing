﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using PruebaAPI.Infrastructure;
using PruebaAPI.Models;
using Entities;
using System.Threading.Tasks;

namespace PruebaAPI.Controllers
{
    [Authorize]
    [RoutePrefix("api/tests")]
    public class ApplicationTestsController : BaseApiController
    {
        // GET: api/ApplicationTests
        [Route("asAdmin/all")]
        public IHttpActionResult GetTests()
        {
            return Ok(this.AppTestManager.GetAll().ToList().Select(x => this.TheModelFactory.Create(x, true)));
        }

        [Route("all")]
        public IHttpActionResult GetTestsForUser()
        {
            List<ApplicationTest> tests = this.AppTestManager.GetTests(this.GetLoggedInUser().Id)
                .ToList();
            if(tests.Count > 0)
            {
                return Ok(tests.Select(x => this.TheModelFactory.Create(x, false)));
            }
            return BadRequest();
        }

        // GET: api/ApplicationTests/5
        [Authorize(Roles ="Admin")]
        [Route("asAdmin/single/{id}", Name = "AdminGetTestById")]
        [ResponseType(typeof(ApplicationTest))]
        public IHttpActionResult GetApplicationTest(int id)
        {
            ApplicationTest applicationTest = this.AppTestManager.GetSingle(id);
            
            if (applicationTest == null)
            {
                return NotFound();
            }

            return Ok(this.TheModelFactory.Create(applicationTest, false));
        }

        
        [Route("single/{id}", Name = "GetTestById")]
        [ResponseType(typeof(ApplicationTest))]
        public IHttpActionResult GetATest(int id)
        {      

            ApplicationTest applicationTest = 
                this.AppTestManager.GetTest(id, this.GetLoggedInUser().Id);

            if (applicationTest == null)
            {
                return NotFound();
            }

            return Ok(this.TheModelFactory.Create(applicationTest, false));
        }

        /// <summary>
        /// Helper-method for retreiving the logged in user from the database,
        /// based on the requests current user.
        /// </summary>
        /// <returns></returns>
        private ApplicationUser GetLoggedInUser()
        {
            return this.AppUserManager.Users
                .FirstOrDefault(x => x.UserName == User.Identity.Name);            
        }


        // PUT: api/ApplicationTests/5

        
        [Route("update/{id}")]
        [ResponseType(typeof(ApplicationTest))]
        [HttpPut]
        public IHttpActionResult PutApplicationTest(int id, TestEditBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != model.TestId)
            {
                return BadRequest("ID mismatch");
            }
            var template = this.MailTempManager.GetSingle(model.TemplateId);
            if(template == null)
            {
                return BadRequest("No template with ID");
            }
            if (this.AppTestManager.UserOwnsTest(id, GetLoggedInUser().Id))
            {
                //Entity lists for conversion from ID to entity
                List<ApplicationUser> customers = new List<ApplicationUser>();              

                //Get all users with the IDs
                foreach (var custId in model.CustomerIds)
                {
                    customers.Add(
                        this.AppUserManager.Users.FirstOrDefault(x => x.Id == custId)
                        //new ApplicationUser() {Id = custId }
                        );
                }
                
                

                //Get the Test with the given ID
                ApplicationTest applicationTest = this.AppTestManager.GetSingle(model.TestId);
                //Set new values
                applicationTest.Id = model.TestId;
                applicationTest.TestName = model.TestName;
                applicationTest.MailTemplate = template;
                applicationTest.Customers = customers;
                applicationTest.Recepients = model.Recepients;

                try
                {
                    var test = this.AppTestManager.Edit(model.TestId, applicationTest);
                    if (test == null)
                    {
                        return BadRequest();
                    }
                    return Ok(this.TheModelFactory.Create(test, true));
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }
            return BadRequest("Something went wrong");
        }

        // POST: api/ApplicationTests        
        [Route("create")]
        //[ResponseType(typeof(ApplicationTest))]
        [HttpPost]
        public IHttpActionResult PostApplicationTest(TestCreateBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            //if(this.AppUserManager.Users.AsNoTracking().Where(x => x.Id == model.RetailerId).Count() == 0)
            //{
            //    return BadRequest("No retailer found with id");
            //}

            //Get customers by ID
            List<ApplicationUser> customers = new List<ApplicationUser>();
                foreach (string id in model.CustomerIds)
                {
                    customers.Add(this.AppUserManager.Users.FirstOrDefault(user => user.Id == id));
                }

            //Get MailTemplate
            MailTemplate template = this.MailTempManager.GetSingle(model.TemplateId);
            if (template == null)
                return BadRequest("No template found");

            //Get retailer by ID
            ApplicationUser retailer = this.GetLoggedInUser();
            //ApplicationUser retailer = this.AppUserManager.Users.FirstOrDefault(user => user.Id == model.RetailerId);
            //if (retailer == null)
            //    return BadRequest("No retailer supplied");
            

            ApplicationTest applicationTest = new ApplicationTest()
            {                
                CreateDate = DateTime.Now.Date,                
                Customers = customers,
                Retailer = retailer,
                TestName = model.TestName,
                Recepients = model.Recepients,
                MailTemplate = template
            };            
            try
            {
                var created = this.AppTestManager.Create(applicationTest);
                var returnTest = this.TheModelFactory.Create(created, true);
                
                return CreatedAtRoute("GetTestById", new { controller = "ApplicationTests", id = returnTest.Id }, returnTest);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }                           
            
        }

        [Authorize(Roles = "Retailer")]
        [Route("stop/{id}")]
        [HttpGet]
        public IHttpActionResult StopTest(int id)
        {
            if (this.AppTestManager.UserOwnsTest(id, this.GetLoggedInUser().Id))
            {
                var started = false;
                ;
                try
                {
                     started = this.AppTestManager.StopTest(id);
                }
                catch(Exception e)
                {
                    return BadRequest(e.Message);
                }
                
                if (started)
                {
                    return Ok($"Test {id} has been stopped");
                }
                else
                {
                    return BadRequest("Something went wrong");
                }
            }
            return BadRequest("You do not own the test you're trying to stop");
        }

        [Authorize(Roles = "Retailer")]
        [Route("start/{id}")]
        [HttpGet]
        [ResponseType(typeof(List<string>))]
        public IHttpActionResult StartTest(int id)
        {
            if(this.AppTestManager.UserOwnsTest(id, this.GetLoggedInUser().Id))
            {
                var test = this.AppTestManager.GetSingle(id);
                var template = test.MailTemplate;

                try
                {
                    var responses = this.MailManager.SendEmailMerge(
                        new EmailMergeModel()
                        {
                            Alias = "mail",
                            Domain = template.SenderDomain,
                            FromName = template.SenderDomainName,
                            Html = template.HtmlContent,
                            Recipients = test.Recepients,
                            Subject = template.Subject,

                        });

                    List<string> responsesText = new List<string>();
                    foreach (var response in responses)
                    {
                        responsesText.Add(response.ToString());
                    }
                    var started = this.AppTestManager.StartTest(id);

                    return Ok(responsesText);
                }
                catch(Exception e)
                {

                    return BadRequest("Something went wrong");
                }
            }
            return BadRequest("You do not own the test you're trying to start");
        }

        // DELETE: api/ApplicationTests/5
        [Authorize(Roles = "Admin")]
        [Route("delete")]
        [ResponseType(typeof(ApplicationTest))]
        [HttpDelete]
        public IHttpActionResult DeleteApplicationTest(int id)
        {
            ApplicationTest applicationTest = this.AppTestManager.GetTest(id, this.GetLoggedInUser().Id);
            if (applicationTest == null)
            {
                return NotFound();
            }

            this.AppTestManager.Delete(applicationTest.Id);           

            return Ok(applicationTest);
        }    



    }
}