﻿using Entities;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using PruebaAPI.Infrastructure;

namespace PruebaAPI.Infrastructure
{
    public class ApplicationTestManager : IApplicationManager<ApplicationTest>
    {
        
        private PruebaDBContext db;
        public ApplicationTestManager()
        {            
            this.db = PruebaDBContext.Create();
        }

        /// <summary>
        /// Returns an ApplicationTest with the given ID. This is for administrators only, as it does not check
        /// if the user requesting it actually owns the test.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ApplicationTest GetSingle(int id)
        {
           return this.GetTestsIncludingRelatedEntities()
                .FirstOrDefault(x => x.Id == id);           
        }

        /// <summary>
        /// Returns an ApplicationTest where the user attached's ID matches the given ID.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userGuid"></param>
        /// <returns></returns>
        public ApplicationTest GetTest(int id, string userGuid)
        {
           return this.GetTestsIncludingRelatedEntities()
                .FirstOrDefault(x => (x.Id == id && x.Retailer.Id == userGuid));           
        }

        /// <summary>
        /// Returns all tests. Should only be used by administrators as there is no 
        /// ownership check
        /// </summary>
        /// <returns></returns>
        public IQueryable<ApplicationTest> GetAll()
        {
            return this.GetTestsIncludingRelatedEntities().AsQueryable();
        }

        /// <summary>
        /// Returns all tests for a given user id.
        /// </summary>
        /// <param name="userGuid"></param>
        /// <returns></returns>
        public IQueryable<ApplicationTest> GetTests(string userGuid)
        {
            List<ApplicationTest> testsForId = this.GetTestsIncludingRelatedEntities()                
                .FindAll(test => test.Retailer.Id == userGuid);

            return testsForId.AsQueryable<ApplicationTest>();
        }

        public ApplicationTest GetTestByRecepientId(int id)
        {
            return this.GetTestsIncludingRelatedEntities().First(x => x.Recepients.Count(y => y.Id == id) > 0);
        }


        private List<ApplicationTest> GetTestsIncludingRelatedEntities()
        {
            return this.db.Tests.Include(test => test.Retailer)
                .Include(test => test.Recepients)
                .Include(test => test.Customers)
                .Include(test => test.MailTemplate)
                .ToList();
        }

        private bool ApplicationTestExists(int id)
        {
            return this.db.Tests.Count(e => e.Id == id) > 0;
        }

        public ApplicationTest Edit(int id, ApplicationTest testToEdit, string userGuid)
        {
            ApplicationTest test = this.db.Tests.FirstOrDefault(x => (x.Id == id && x.Retailer.Id == userGuid));
            this.Delete(test.Id);
            return this.Create(testToEdit);
            //if (ApplicationTestExists(id) && test != null)
            //{
            //test.Customers = testToEdit.Customers;
            //test.MailTemplate = testToEdit.MailTemplate;
            //test.Recepients = testToEdit.Recepients;
            //test.TestName = testToEdit.TestName;             

            //List<ApplicationUser> customers = new List<ApplicationUser>();
            //foreach (var customer in testToEdit.Customers)
            //{
            //var count = db.Users.AsNoTracking().Where(x => x.Id == customer.Id).Count();
            //if (count == 0)
            //{
            //this.db.Users.Attach(customer);
            //}
            //customers.Add(this.db.Users.FirstOrDefault(x => x.Id == customer.Id));
            //}                
            //test.Customers = customers;

            //List<ApplicationRecepient> recepients = new List<ApplicationRecepient>();
            //foreach(var recepient in testToEdit.Recepients)
            //{
            //recepients.Add(this.db.Recepients.FirstOrDefault(x => x.Id == recepient.Id));
            //}
            //test.Recepients = recepients;


            //this.db.Entry(test).State = EntityState.Modified;
            //this.db.Users.Attach(testToEdit.Retailer);


            //try
            //{
            //db.SaveChanges();
            //return testToEdit;

            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //if (!ApplicationTestExists(testToEdit.Id))
            //{
            //throw new Exception("Test does not exsist");
            //}
            //else
            //{
            //throw;
            //}
            //}                
            //}
            //return null;
        }        

        public ApplicationTest Create(ApplicationTest test)
        {            
            var added = db.Tests.Add(test);
            db.Users.Attach(added.Retailer);
            foreach(var customer in test.Customers)
            {
                db.Users.Attach(customer);
            }
            db.MailTemplates.Attach(added.MailTemplate);
            try
            {
                db.SaveChanges();
                return added;
            }
            catch (DbEntityValidationException e)
            {                
                throw;
            }
        }

        public void Delete(int id)
        {
            ApplicationTest test = this.db.Tests.FirstOrDefault(x => x.Id == id);
            if(test != null)
            {
                db.Tests.Remove(test);
                try
                {
                    db.SaveChanges();
                }
                catch(DbUpdateConcurrencyException e)
                {
                    if(!ApplicationTestExists(id))
                    {
                        throw new Exception("Test does not exsist");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            
            
        }

        /// <summary>
        /// Gets an ApplicationTest with a given ID, sets its StartDate to the current time and calls the Edit method.
        /// 
        /// returns true if the edit succeeded, otherwise false.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool StartTest(int id)
        {
            ApplicationTest test = this.db.Tests.FirstOrDefault(x => (x.Id == id));
            if (test.StartDate != null)
            {
                throw new Exception("Test is already started");
            }
            test.StartDate = DateTime.Now;
            var returnTest = this.Edit(id, test);
            return returnTest != null ? true : false;                      
        }

        public bool StopTest(int id)
        {
            
            ApplicationTest test = this.db.Tests.FirstOrDefault(x => (x.Id == id));
            if (test.EndDate != null)
            {
                throw new Exception("Test is already ended");
            }
            test.EndDate = DateTime.Now;
            var returnTest = this.Edit(id, test);
            return returnTest != null ? true : false;
        }

        public ApplicationTest Edit(int id, ApplicationTest toEdit)
        {
            ApplicationTest test = this.db.Tests.FirstOrDefault(x => (x.Id == id));
            
            if (test != null)
            {
                if (toEdit.Customers != null)
                {
                    foreach (var customer in toEdit.Customers)
                    {
                        this.db.Users.Attach(customer);
                    }
                }
                this.db.Entry(toEdit).State = EntityState.Modified;                
                try
                {
                    db.SaveChanges();
                    return test;
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ApplicationTestExists(toEdit.Id))
                    {
                        throw new Exception("Test doesnt exsist with the given ID");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return null;
        }

        public bool UserOwnsTest(int testId, string userGuid)
        {
            return this.db.Tests
                .AsNoTracking()
                .Where(x => x.Id == testId && x.Retailer.Id == userGuid)
                .Count() > 0;
        }
    }
}