﻿using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using PruebaAPI.Models;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace PruebaAPI.Infrastructure
{
    public class MailManager
    {
        private PruebaDBContext db;
        private SendGridClient client;


        public MailManager()
        {
            this.db = new PruebaDBContext();
            var apiKey = ConfigurationManager.AppSettings["as:SendAPIKey"];
            client = new SendGridClient(apiKey);            
        }

        public List<MailResponse> SendEmailMerge(EmailMergeModel model)
        {
            var responses = new List<MailResponse>();
            var from = new EmailAddress(model.Email(), model.FromName);
            var subject = model.Subject;
            var content = model.Html;

            foreach (var address in model.Recipients)
            {
                var replacedHtml = new UrlReplacer().ReplaceUrlInHtml(content, $"https://{model.Domain}/api/mail/click/{address.Id}");                
                responses.Add(SendEmail(new MailModel()
                {
                    Subject = subject,
                    FromEmail = from.Email,
                    FromName = from.Name,
                    Html = replacedHtml,
                    MailRecepient = address
                }));
            }
            return responses;
        }

        public MailResponse SendEmail(MailModel mailModel)
        {
            var to = new EmailAddress(mailModel.MailRecepient.GetEmail(), mailModel.MailRecepient.GetName());

            var msg = MailHelper.CreateSingleEmail(
                new EmailAddress(mailModel.FromEmail, mailModel.FromName),
                to, mailModel.Subject, null, mailModel.Html);

            MailResponse response = new MailResponse()
            {
                Response = client.SendEmailAsync(msg),
                ToAddress = to
            };
            return response;
        }
    }

    public class MailResponse
    {
        public Task<Response> Response { get; set; }
        public EmailAddress ToAddress { get; set; }

        public override string ToString()
        {
            string status = Response.Result.StatusCode == HttpStatusCode.OK ? "Sent" : "Not sent";
            return $"{status} - {ToAddress.Name}, {ToAddress.Email}";
        }

    }
}