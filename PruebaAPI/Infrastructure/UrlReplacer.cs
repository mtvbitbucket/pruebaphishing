﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace PruebaAPI.Infrastructure
{
    public class UrlReplacer
    {

        /// <summary>
        /// Finds the first anchor in an HtmlString and replaces the href value with a given url
        /// </summary>
        /// <param name="html"></param>
        /// <param name="url"></param>
        /// <returns></returns>
        public string ReplaceUrlInHtml(string html, string url)
        {
            var doc = new HtmlDocument();            
            doc.LoadHtml(html);
            if(doc.ParseErrors != null && doc.ParseErrors.Count() > 0)
            {
                string msg = "Error replacing URLs:\n";
                foreach(var error in doc.ParseErrors)
                {
                    msg = msg
                        + "Line: " + error.Line
                        + "; Position: " + error.LinePosition
                        + "; Error: " + error.Reason
                        + "\n";
                }
                throw new Exception(msg);
            }
            
            //Get root document
            var root = doc.DocumentNode;

            //Get the first occurance of an anchor
            var nodes = root.SelectNodes("//a");
            foreach(var node in nodes)
            {
                var redirect = Uri.EscapeDataString(node.GetAttributeValue("href", ""));

                //Set the href value to the given url
                node.SetAttributeValue("href", $"{url}?redirect={redirect}");
            }
            

            //Write the edited HtmlDocument to a string
            string returnHtml = null;
            using (StringWriter writer = new StringWriter())
            {
                doc.Save(writer);
                returnHtml = writer.ToString();
            }
            if(returnHtml == null)
            {
                throw new Exception("Error replacing URL");
            }

            //Return the edited HtmlString
            return returnHtml;
        }
    }
}