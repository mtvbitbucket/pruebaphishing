﻿using Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace PruebaAPI.Infrastructure
{
    public class MailTemplateManager : IApplicationManager<MailTemplate>
    {
        private PruebaDBContext db;

        public MailTemplateManager()
        {
            this.db = PruebaDBContext.Create();
        }

        public MailTemplate Create(MailTemplate entity)
        {
            
            var added = db.MailTemplates.Add(entity);
            if (entity.ApplicationTests != null)
            {
                foreach (var test in entity.ApplicationTests)
                {
                    db.Tests.Attach(test);
                }
            }
            try
            {
                db.SaveChanges();
                return added;
            }
            catch (DbEntityValidationException e)
            {
                throw;
            }

        }

        public void Delete(int id)
        {
            MailTemplate template = this.db.MailTemplates.FirstOrDefault(x => (x.Id == id));
            if(template != null)
            {
                db.MailTemplates.Remove(template);
                try
                {
                    db.SaveChanges();
                }
                catch(DbUpdateConcurrencyException e)
                {
                    if(!MailTemplateExists(id))
                    {
                        throw new Exception("MailTemplate does not exsist");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        public MailTemplate Edit(int id, MailTemplate toEdit)
        {
            MailTemplate template = this.db.MailTemplates.FirstOrDefault(x => (x.Id == id));
            
            if (template != null)
            {
                template.ApplicationTests = toEdit.ApplicationTests;
                template.HtmlContent = toEdit.HtmlContent;
                template.SenderDomain = toEdit.SenderDomain;
                template.SenderDomainName = toEdit.SenderDomainName;
                template.Subject = toEdit.Subject;

                this.db.Entry(template).State = EntityState.Modified;                

                try
                {
                    db.SaveChanges();
                    return template;
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!MailTemplateExists(toEdit.Id))
                    {
                        throw new Exception("MailTemplate does not exsist.");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            return null;
        }

        public IQueryable<MailTemplate> GetAll()
        {
            return this.db.MailTemplates;
        }

        public MailTemplate GetSingle(int id)
        {
            return this.db.MailTemplates.FirstOrDefault(m => m.Id == id);
        }

        private bool MailTemplateExists(int id)
        {
            return this.db.MailTemplates.Count(e => e.Id == id) > 0;
        }
    }
}