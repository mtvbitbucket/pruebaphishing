﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaAPI.Infrastructure
{
    public interface IApplicationManager<T>
    {
        /// <summary>
        /// Returns all entities of type T
        /// </summary>
        /// <returns></returns>
        IQueryable<T> GetAll();        
        
        /// <summary>
        /// Returns a single entity of type T where T's ID matches the given ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        T GetSingle(int id);

        /// <summary>
        /// Edits an entity of Type T where T's ID matches the given ID        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="toEdit"></param>
        /// <returns></returns>
        T Edit(int id, T toEdit);

        /// <summary>
        /// Deletes a single entity of type T where T's ID matches the given ID
        /// </summary>
        /// <param name="id"></param>
        void Delete(int id);
        /// <summary>
        /// Creates an entity T with the given information
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        T Create(T entity);
    }
}
