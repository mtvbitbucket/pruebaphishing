﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using Entities;

namespace PruebaAPI.Infrastructure
{
    public class PruebaDBContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<ApplicationTest> Tests { get; set; }
        public DbSet<ApplicationRecepient> Recepients { get; set; }
        public DbSet<MailTemplate> MailTemplates { get; set; }

        public PruebaDBContext() : base("DefaultConnection", throwIfV1Schema: false)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {            

            //One-to-many
            //An ApplicationTest has many ApplicationRecepients which have a required ApplicationTest
            modelBuilder.Entity<ApplicationTest>()
                .HasMany<ApplicationRecepient>(t => t.Recepients)
                .WithRequired(r => r.Test).WillCascadeOnDelete(false);

            //An ApplicationTest has a required ApplicationUser (Retailer)
            modelBuilder.Entity<ApplicationUser>()
                .HasMany(u => u.Tests)
                .WithRequired(t => t.Retailer);

            //An ApplicationTest has a required MailTemplate, which has many ApplicationTests
            modelBuilder.Entity<ApplicationTest>()
                .HasRequired(t => t.MailTemplate)
                .WithMany(m => m.ApplicationTests);
            
            //Many-to-many
            //An ApplicationUser can have many ApplicationTests, which can have many ApplicationUsers(Customers)            
            modelBuilder.Entity<ApplicationUser>()
                .HasMany<ApplicationTest>(u => u.Tests)
                .WithMany(t => t.Customers)
                .Map(x =>
                {
                    x.MapLeftKey("ApplicationUserId");
                    x.MapRightKey("ApplicationTestId");
                    x.ToTable("ApplicationUserApplicationTest");
                });

            base.OnModelCreating(modelBuilder);
        }

        public static PruebaDBContext Create()
        {
            return new PruebaDBContext();
        }
    }
}