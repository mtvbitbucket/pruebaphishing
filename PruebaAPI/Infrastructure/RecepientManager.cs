﻿using Entities;
using PruebaAPI.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace PruebaAPI.Infrastructure
{
    public class ApplicationRecepientManager : IApplicationManager<ApplicationRecepient>
    {
        private PruebaDBContext db;
        public ApplicationRecepientManager() 
        {
            this.db = PruebaDBContext.Create();
        }

        public void addClick(int id)
        {
            ApplicationRecepient recepient = this.db.Recepients.FirstOrDefault(x => x.Id == id);
            recepient.ClickCount++;
            this.Edit(id, recepient);
        }
        public void addOpen(int id)
        {
            ApplicationRecepient recepient = this.db.Recepients.FirstOrDefault(x => x.Id == id);
            recepient.OpenCount++;
            this.Edit(id, recepient);
        }
        public void addSubmit(int id)
        {
            ApplicationRecepient recepient = this.db.Recepients.FirstOrDefault(x => x.Id == id);
            recepient.SubmitCount++;
            this.Edit(id, recepient);
        }

        public ApplicationRecepient Create(ApplicationRecepient entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            ApplicationRecepient recepient = this.db.Recepients.FirstOrDefault(x => x.Id == id);
            if (recepient != null)
            {
                db.Recepients.Remove(recepient);
                try
                {
                    db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException e)
                {
                    if (!ApplicationRecepientExsists(id))
                    {
                        throw new Exception("Test does not exsist");
                    }
                    else
                    {
                        throw;
                    }
                }
            }
        }

        private bool ApplicationRecepientExsists(int id)
        {
            return this.db.Recepients.Count(e => e.Id == id) > 0;
        }

        public ApplicationRecepient Edit(int id, ApplicationRecepient toEdit)
        {
            ApplicationRecepient recepient = this.db.Recepients.FirstOrDefault(x => x.Id == id);
            if(ApplicationRecepientExsists(id) && recepient != null)
            {
                this.db.Entry(toEdit);
            }
            try
            {
                db.SaveChanges();
                return toEdit;

            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ApplicationRecepientExsists(toEdit.Id))
                {
                    throw new Exception("Test does not exsist");
                }
                else
                {
                    throw;
                }
            }

        }

        public IQueryable<ApplicationRecepient> GetAll()
        {
            return db.Recepients;
        }

        public ApplicationRecepient GetSingle(int id)
        {
            return db.Recepients.FirstOrDefault(x => x.Id == id);
        }
    }
}