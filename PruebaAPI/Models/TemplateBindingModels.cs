﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaAPI.Models
{
    public class TemplateCreateBindingModel
    {
        //Domain the email is sent from
        public string SenderDomain { get; set; }

        //Name of the sender
        public string SenderDomainName { get; set; }

        //HTML Content
        public string HtmlContent { get; set; }

        //Subject of the email
        public string Subject { get; set; }

        public string RedirectUrl { get; set; }
    }

    public class TemplateEditBindingModel
    {
        public int Id { get; set; }
        //Domain the email is sent from
        public string SenderDomain { get; set; }

        //Name of the sender
        public string SenderDomainName { get; set; }

        //HTML Content
        public string HtmlContent { get; set; }

        //Subject of the email
        public string Subject { get; set; }
        public string RedirectUrl { get; set; }
    }
}