﻿using Entities;
using PruebaAPI.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PruebaAPI.Models
{
    public class TestCreateBindingModel
    {
        //[Required]
        [Display(Name = "Testname")]
        public string TestName { get; set; }
        //[Required]
        [Display(Name = "Customerids")]
        public List<string> CustomerIds { get;  set; }
        [Display(Name = "Templateid")]
        public int TemplateId { get; set; }
        [Display(Name = "Recepients")]
        public List<ApplicationRecepient> Recepients { get; set; }
    }
    public class TestEditBindingModel
    {
        [Required]
        public int TestId { get; set; }
        [Required]
        public string TestName { get; set; }
        [Required]
        public int TemplateId { get; set; }
        [Required]
        public List<string> CustomerIds { get; set; }      
        [Required]  
        public List<ApplicationRecepient> Recepients { get; set; }

    }
}