﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http.Routing;
using Microsoft.AspNet.Identity.EntityFramework;
using PruebaAPI.Infrastructure;
using Entities;

namespace PruebaAPI.Models
{
    public class ModelFactory
    {
        private UrlHelper _UrlHelper;
        private ApplicationUserManager _AppUserManager;

        public ModelFactory(HttpRequestMessage request, ApplicationUserManager appUserManager)
        {
            _UrlHelper = new UrlHelper(request);
            _AppUserManager = appUserManager;
        }

        public List<MailTemplateReturnModel> CreateList(List<MailTemplate> list)
        {
            var converted = new List<MailTemplateReturnModel>();
            foreach(var template in list)
            {
                converted.Add(this.Create(template));
            }
            return converted;
        }

        public UserReturnModel Create(ApplicationUser appUser)
        {
            return new UserReturnModel
            {
                Url = _UrlHelper.Link("GetUserById", new { id = appUser.Id }),
                UserName = appUser.UserName,
                Id = appUser.Id,
                FirstName = appUser.FirstName,
                LastName = appUser.LastName,
                Email = appUser.Email,
                EmailConfirmed = appUser.EmailConfirmed,
                Level = appUser.Level,
                Company = appUser.Company,
                JoinDate = appUser.JoinDate,
                Roles = _AppUserManager.GetRolesAsync(appUser.Id).Result,
                Claims = _AppUserManager.GetClaimsAsync(appUser.Id).Result,
                RetailerId = appUser.RetailerId
            };
        }

        public RoleReturnModel Create(IdentityRole appRole)
        {

            return new RoleReturnModel
            {
                Url = _UrlHelper.Link("GetRoleById", new { id = appRole.Id }),
                Id = appRole.Id,
                Name = appRole.Name
            };
        }

        /// <summary>
        /// Creates a TestReturnModel for an ApplicationTest
        /// 
        /// This ensures that all ApplicationUsers and Recepients tied to the test
        /// are not including related entities, and other properties that we don't
        /// want returned out of the system.
        /// 
        /// The method automatically calls the Create-method for all ApplicationUsers
        /// and ApplicationRecepients in the ApplicationTest.
        /// </summary>
        /// <param name="test"></param>
        /// <returns></returns>
        public TestReturnModel Create(ApplicationTest test, bool idOnly)
        {
            //Convert all customers to models
            List<UserReturnModel> users = (test.Customers == null) 
                ? new List<UserReturnModel>() : convertUsers(test.Customers);

            //Convert all recepients to models
            List<RecepientReturnModel> recepients = (test.Recepients == null)
                ? new List<RecepientReturnModel>() : convertRecepients(test.Recepients);

            //Convert retailer to model                     

            return new TestReturnModel
            {
                Url = _UrlHelper.Link("GetTestById", new { id = test.Id }),                
                CreateDate = test.CreateDate,
                EndDate = test.EndDate??DateTime.Now.Date,
                Id = test.Id,
                StartDate = test.StartDate??DateTime.Now.Date,
                TestName = test.TestName,
                Customers = users,
                Recepients = recepients,
                Retailer = test.Retailer.Id,

            };
        }

        /// <summary>
        /// Converts an ApplicationRecepient into a RecepientReturnModel, removing
        /// the ApplicationTest property, and only returning the ID of the test instead.
        /// </summary>
        /// <param name="recepient"></param>
        /// <returns></returns>
        public RecepientReturnModel Create(ApplicationRecepient recepient)
        {
            return new RecepientReturnModel
            {
                id = recepient.Id,
                Name = recepient.Name,
                Email = recepient.Email,
                ClickCount = recepient.ClickCount,
                OpenCount = recepient.OpenCount,
                TestId = recepient.Test.Id,
                Url = "XXX"                
            };
        }

        public MailTemplateReturnModel Create(MailTemplate template)
        {
            return new MailTemplateReturnModel()
            {
                Id = template.Id,
                HtmlContent = template.HtmlContent,
                SenderDomain = template.SenderDomain,
                SenderDomainName = template.SenderDomainName,
                Subject = template.Subject,
                RedirectUrl = template.RedirectUrl
            };
        }

        /// <summary>
        /// Converts given ApplicationUsers into UserReturnModels, thus limiting properties returned
        /// </summary>
        /// <param name="recepients"></param>
        /// <returns></returns>
        private List<UserReturnModel> convertUsers(List<ApplicationUser> users)
        {
            List<UserReturnModel> converted = new List<UserReturnModel>();
            foreach (var user in users)
            {
                converted.Add(Create(user));
            }
            return converted;
        }

        

        /// <summary>
        /// Converts given ApplicationRecepients into RecepientReturnModels, thus limiting properties returned
        /// </summary>
        /// <param name="recepients"></param>
        /// <returns></returns>
        private List<RecepientReturnModel> convertRecepients(List<ApplicationRecepient> recepients)
        {
            List<RecepientReturnModel> converted = new List<RecepientReturnModel>();
            foreach(var recepient in recepients)
            {
                converted.Add(Create(recepient));
            }
            return converted;
        }
    }

    public class RecepientReturnModel
    {
        public string Url { get; set; }
        public int id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public int ClickCount { get; set; }
        public int OpenCount { get; set; }
        public int TestId { get; set; }

    }
    

    public class TestReturnModel
    {
        public string Url { get; set; }
        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string TestName { get; set; }
      
        public List<UserReturnModel> Customers { get; set; }
        public List<RecepientReturnModel> Recepients { get; set; }
        public string Retailer { get; set; }
    }

    
    public class UserReturnModel
    {
        public string Url { get; set; }
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string Company { get; set; }
        public int Level { get; set; }
        public DateTime JoinDate { get; set; }
        public IList<string> Roles { get; set; }
        public IList<System.Security.Claims.Claim> Claims { get; set; }
        public string RetailerId { get; set; }
    }

    public class RoleReturnModel
    {
        public string Url { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class MailTemplateReturnModel
    {
        public int Id { get; set; }
        //Domain the email is sent from
        public string SenderDomain { get; set; }

        //Name of the sender
        public string SenderDomainName { get; set; }

        //HTML Content
        public string HtmlContent { get; set; }

        //Subject of the email
        public string Subject { get; set; }

        public string RedirectUrl { get; set; }
    }
}
    
