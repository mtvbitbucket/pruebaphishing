﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities;

namespace PruebaAPI.Models
{
    public class EmailMergeModel
    {
        /// <summary>
        /// The email address that the email is sent to
        /// </summary>
        public List<ApplicationRecepient> Recipients { get; set; }
        
        /// <summary>
        /// The HTML body of the email
        /// </summary>
        public string Html { get; set; }

        /// <summary>
        /// Alias of the sender
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// Domain of the sender
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// The name of the sender
        /// </summary>
        public string FromName { get; set; }

        /// <summary>
        /// The Text body of the email
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Number of attachments included in email
        /// </summary>
        public int Attachments { get; set; }

        /// <summary>
        /// The subject of the email
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// The results of the Sender Policy Framework verification of the message sender and receiving IP address.
        /// </summary>
        public string Spf { get; set; }

        /// <summary>
        /// The email address of the sender, "{Alias}@{Domain}".
        /// </summary>
        public string Email()
        {
            return $"{this.Alias}@{this.Domain}";
        }
    }

    public class MailModel
    {
        public ApplicationRecepient MailRecepient { get; set; }
        public string FromName { get; set; }
        public string FromEmail { get; set; }
        public string Html { get; set; }
        public string Text { get; set; }
        public string Subject { get; set; }
    }

    
}